## Introduction

These are all the RaspberryPi commands that 
need to be run in order for the sensors to
work properly.

## Raspberry Pi Commands

```
sudo python setup.py install
sudo ./AdafruitDHT.py 22 23
pip install --upgrade oauth2client
pip install PyOpenSSL
sudo apt-get install python-pip
sudo pip install gspread oauth2client
sudo apt-get --reinstall install python-pyasn1 python-pyasn1-modules
sudo apt-get update
sudo apt-get install <pyasn1>
sudo apt-get install fail2ban
sudo apt-get remove python-pyasn1_python-pyasn1-modules_
sudo apt-get install pip
sudo apt-get update && sudo apt-get install arduino
sudo apt-get install tightvncserver
sudo apt-get install git-all
sudo apt-get install dnsmasq
sudo apt-get install build-essential python-dev
sudo apt-get install nmap
pip install pyasn1-modules
pip install AWSIoTPythonSDK
sudo apt-get install build-essential python-dev
git clone https://github.com/adafruit/Adafruit_Python_DHT.git && cd Adafruit_Python_DHT && sudo python setup.py install
git clone https://github.com/Azure-Samples/powerbi-python-iot-client.git install
git clone https://github.com/adafruit/Adafruit_Python_DHT.git
git clone https://github.com/etingof/pyasn1.git
iptables -L

netstat-ulnp | grep ¨:53 ¨
Serial.println("Initialized");
//Serial.println(value);
//Serial.println(dBadjustment);
Serial.println(dBlevel);
Serial.println("Quiet.");
Serial.println("Moderate.");
Serial.println("Loud.")
```

## iptables rules.v4

```
*filter
:INPUT ACCEPT [13152:1060273]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [8944:1941384]
:f2b-sshd - [0:0]

## Safeguards for loopback, ping, + established/related traffic
-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

## Set up Fail2Ban Bruteforce blocker
-A INPUT -p tcp -m multiport --dports 22 -j f2b-sshd

## Safeguards for loopback, ping, + established/related traffic
-A INPUT -i lo -j ACCEPT
-A INPUT -p icmp --icmp-type any -j ACCEPT

## Safe Prorocols To Allow In
-A INPUT -p tcp -m multiport --dports 22 -j ACCEPT

## Reject everything else
-A f2b-sshd -j RETURN
-A INPUT -j REJECT
COMMIT
# Completed on Fri Mar 30 10:46:22 2018
```

## iptables rules.v6

```
# Generated by ip6tables-save v1.6.0 on Fri Apr  6 10:37:52 2018
*filter
:INPUT ACCEPT [49:5382]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [111:7992]

-A INPUT -j DROP

COMMIT
# Completed on Fri Apr  6 10:37:52 2018
```