"""
Python sample for Raspberry Pi which reads temperature and humidity values from
a DHT22 sensor, and sends that data to Power BI for use in a streaming dataset.
"""

import urllib, urllib2, time
from datetime import datetime
import Adafruit_DHT as dht
#import this for serial communicatino with arduino
import serial
# type of sensor that we're using
SENSOR = dht.DHT22

#This block initializes serial commuincation
ser = serial.Serial('/dev/ttyACM0',9600)
ser.readline()

# pin which reads the temperature and humidity from sensor
PIN = 23

# REST API endpoint, given to you when you create an API streaming dataset
# Will be of the format: https://api.powerbi.com/beta/<tenant id>/datasets/< dataset id>/rows?key=<key id>
REST_API_URL = "https://api.powerbi.com/beta/60956884-10ad-40fa-863d-4f32c1e3a37a/datasets/bc3631e4-3196-4866-817c-32844301bb22/rows?key=
#Contact information for the key is provided in the ReadMe file

# Gather temperature and sensor data and push to Power BI REST API
while True:
        try:
                # read and print out humidity and temperature from sensor
                humidity,temp = dht.read_retry(SENSOR, PIN)
                value = int(ser.readline())
                print 'Temp={0:0.1f}*C Humidity={1:0.1f}% Noise={2:0.1f}dB'.format(temp, humidity, value)
                # ensure that timestamp string is formatted properly
                now = datetime.strftime(datetime.now(), "%Y-%m-%dT%H:%M:%S%Z")

                # data that we're sending to Power BI REST API
                data = '[{{ "timestamp": "{0}", "temperature": "{1:0.1f}", "humidity": "{2:0.1f}", "Noise": "{3:0.1f}" }}]'.format(now, temp, humidity, value)

                # make HTTP POST request to Power BI REST API
                req = urllib2.Request(REST_API_URL, data)
                response = urllib2.urlopen(req)
                print("POST request to Power BI with data:{0}".format(data))
                print("Response: HTTP {0} {1}\n".format(response.getcode(), response.read()))

                time.sleep(60)
        except urllib2.HTTPError as e:
                print("HTTP Error: {0} - {1}".format(e.code, e.reason))
        except urllib2.URLError as e:
                print("URL Error: {0}".format(e.reason))
        except Exception as e:
                print("General Exception: {0}".format(e))
